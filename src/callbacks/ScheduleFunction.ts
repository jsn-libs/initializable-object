import {AnyCallback} from './AnyCallback';

export interface ScheduleFunction{

    (callback: AnyCallback): void;
    (callback: AnyCallback, ...args: any[]): void;

}
