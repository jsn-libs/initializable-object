export interface AnyCallback{

    (...args: any[]): any;

}
