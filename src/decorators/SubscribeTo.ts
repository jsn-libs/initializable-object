import {EventSubscription} from '../types';

export function SubscribeTo(target: string|symbol, event: string|symbol,
                            type: 'on'|'once' = 'on', ignoreDefault: boolean = false): MethodDecorator{
    return function(_target, key, descriptor){
        const subscriptions = (Reflect.getOwnMetadata('EventSubscriptions', _target) || []) as EventSubscription[];

        for(const subscription of subscriptions){
            if(subscription.target === target
                && subscription.type === type
                && subscription.key === key
                && subscription.event === event
                && subscription.ignoreDefault === ignoreDefault)
                return;
        }

        subscriptions.push({
            target: target,
            type: type,
            event: event,
            key: key,
            ignoreDefault: ignoreDefault,
            ctor: _target.constructor.name
        });

        Reflect.defineMetadata('EventSubscriptions', subscriptions, _target);
    };
}
