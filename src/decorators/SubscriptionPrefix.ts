import {EventPrefix} from '../types';

export function SubscriptionPrefix(target: string|symbol, prefix: string): ClassDecorator{
    return function(_target: Function){
        const prefixes = (Reflect.getOwnMetadata('EventPrefixes', _target) || []) as EventPrefix[];

        for(const pref of prefixes){
            if(pref.target === target && pref.prefix === prefix)
                return;
        }

        prefixes.push({
            target: target,
            prefix: prefix,
            ctor: _target.name
        });

        Reflect.defineMetadata('EventPrefixes', prefixes, _target);
    };
}
