export interface IDisposable{

    /**
     * Method SHOULD begin disposal process
     * @return {void}
     */
    dispose(): void;

}
