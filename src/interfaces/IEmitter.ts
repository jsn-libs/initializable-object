import {AnyCallback} from '../callbacks';

export interface IEmitter{

    on: { (event: string, handler: AnyCallback): void; };
    once: { (event: string, handler: AnyCallback): void; };
    off: { (event: string, handler: AnyCallback): void; };

}
