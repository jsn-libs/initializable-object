export interface ILogger{

    debug(message: string, ...args: any[]): void;
    info(message: string, ...args: any[]): void;
    notice(message: string, ...args: any[]): void;
    warning(message: string, ...args: any[]): void;
    error(message: string, ...args: any[]): void;

}
