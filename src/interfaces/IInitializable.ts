export interface IInitializable{

    /**
     * Method SHOULD begin initialization process
     * @return {void}
     */
    initialize(): void;

}
