export type EventPrefix = {
    target: string|symbol;
    prefix: string;
    ctor: string;
}
