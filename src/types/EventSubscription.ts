export type EventSubscription = {
    target: string|symbol;
    type: 'on'|'once';
    event: string|symbol;
    ignoreDefault: boolean;

    key: string|symbol;
    ctor: string;
};
