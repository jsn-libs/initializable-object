import {AnyCallback} from '../callbacks';

/**
 * @param {AnyCallback} fn
 * @param {any[]} args
 * @param thisArg
 * @return {Promise<ReturnType<T>>}
 */
export async function callFn<T extends AnyCallback = AnyCallback>(
    fn: AnyCallback, args: any[] = [], thisArg: any = null
): Promise<ReturnType<T>>{
    const result = fn.apply(thisArg, args);
    if(result && result instanceof Promise){
        try{
            return await result;
        }catch(e){
            throw e;
        }
    }

    return result;
}
