/**
 * @param {*} target
 * @param {Function} emitFn
 * @param {any[]} bindArgs
 */
export function setEventsProxy(target: any, emitFn: Function, bindArgs: any[] = []): void{
   if(!target || typeof target !== 'object' || !target.emit || typeof target.emit !== 'function')
       return;

   const originalEmit = target.emit;
   target.emit = (event: string, ...args: any[]) => {
       emitFn(event, ...bindArgs, ...args);
       originalEmit(event, ...args);
   };
}
