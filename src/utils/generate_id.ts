import {v4} from 'uuid';

/**
 * Generates UUID v4
 * @return {string}
 */
export function generate_id(): string{
    return v4();
}
