import {EventPrefix, EventSubscription} from '../types';

/**
 * @param {object} target
 * @return {Exclude<EventSubscription, "ignoreDefault" | "ctor">[]}
 */
export function getEventsMetadata(target: object): Exclude<EventSubscription, 'ignoreDefault'|'ctor'>[]{
    const subscriptions: EventSubscription[] = [];

    let o = Object.getPrototypeOf(target);
    while(o !== Object.prototype){

        const childFields = Reflect.getOwnMetadata('EventSubscriptions', o) || [],
            eventPrefixes = (Reflect.getOwnMetadata('EventPrefixes', o.constructor) || []) as EventPrefix[];

        subscriptions.push(...(childFields.map((f: any) => {
            let prefix = '';
            for(const eventPrefix of eventPrefixes){
                if(eventPrefix.target === f.target){
                    prefix = eventPrefix.prefix;
                    break;
                }
            }

            return Object.assign({}, {
                target: f.target,
                type: f.type,
                event: prefix + (prefix !== '' ? ':' : '') + f.event,
                key: f.key
            });
        })));

        o = Object.getPrototypeOf(o);
    }

    return subscriptions;
}
