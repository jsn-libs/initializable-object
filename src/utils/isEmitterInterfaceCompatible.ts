import {IEmitter} from '../interfaces';

/**
 * @param obj
 * @return {obj is IEmitter}
 */
export function isEmitterInterfaceCompatible(obj: any): obj is IEmitter{
    if(!obj || (typeof obj !== 'object' && typeof obj !== 'function'))
        return false;

    if(!obj['on'] || typeof obj['on'] !== 'function')
        return false;

    if(!obj['once'] || typeof obj['once'] !== 'function')
        return false;

    if(!obj['off'] || typeof obj['off'] !== 'function')
        return false;

    return true;
}
