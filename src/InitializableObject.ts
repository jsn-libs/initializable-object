import {EventsAware} from './EventsAware';
import {AnyCallback, VoidCallback} from './callbacks';
import {IDisposable, IEmitter, IInitializable, ILogger} from './interfaces';
import {InitializableObjectState} from './enums';
import {callFn, generate_id, getEventsMetadata, isEmitterInterfaceCompatible} from './utils';
import {EventSubscription} from './types';
import _merge = require('lodash.merge');
import { SHA1 } from 'crypto-js';

export abstract class InitializableObject<
    TOpts extends any = never,
    TEmitEvents extends AnyCallback = AnyCallback,
    TSubscribeEvents extends AnyCallback = AnyCallback
    > extends EventsAware implements IInitializable, IDisposable{

    /**
     * Unique initializable object identifier
     * @return {string}
     */
    public get _id(): string{ return this.__id; }
    private __id: string;

    /**
     * @return {string}
     */
    public get _id_sha1(): string{ return this.__id_sha1; }
    private __id_sha1: string;

    /**
     * @type {ILogger}
     * @protected
     */
    protected readonly logger: ILogger;

    /**
     * @type {Required<TOpts>}
     * @protected
     */
    protected readonly options: Required<TOpts>;

    /**
     * Object state
     * @return {InitializableObjectState}
     */
    public get state(): InitializableObjectState{ return this._state; }
    private _state: InitializableObjectState;

    private _set_initialized_called: boolean;
    private _set_disposed_called: boolean;

    /**
     * @type {Map<EventSubscription, HandlerInfo>}
     * @private
     */
    private readonly _event_handlers: Map<EventSubscription, HandlerInfo>;

    /**
     * Indicates that object is uninitialized
     * @return {boolean}
     */
    public get isUninitialized(): boolean{ return this._state === InitializableObjectState.Uninitialized; }

    /**
     * Indicates that the object is initializing
     * @return {boolean}
     */
    public get isInitializing(): boolean{ return this._state === InitializableObjectState.Initializing; }

    /**
     * Indicates that the object is initialized
     * @return {boolean}
     */
    public get isInitialized(): boolean{ return this._state === InitializableObjectState.Initialized; }

    /**
     * Indicates that the object is disposing and isn't usable anymore
     * @return {boolean}
     */
    public get isDisposing(): boolean{ return this._state === InitializableObjectState.Disposing; }

    /**
     * Indicates that the object is disposed and isn't usable anymore
     * @return {boolean}
     */
    public get isDisposed(): boolean{ return this._state === InitializableObjectState.Disposed; }

    //region Abstract properties

    /**
     * Property indicates that method .setInitialized will be called explicitly
     * @type {boolean}
     * @see setInitialized
     * @protected
     */
    protected abstract readonly is_deferred_initialization: boolean;

    /**
     * Property indicates that method .setDisposed will be called explicitly
     * @type {boolean}
     * @see setDisposed
     * @protected
     */
    protected abstract readonly is_deferred_disposition: boolean;

    //endregion

    /**
     * @param {ILogger} logger
     * @param {TOpts} options
     */
    public constructor(logger: ILogger, options?: TOpts) {
        super(setImmediate);

        this.__id = generate_id();
        this.__id_sha1 = SHA1(this.__id).toString().slice(0, 8);
        this.logger = logger;
        this.options = this.mergeOptions(options);
        this._state = InitializableObjectState.Uninitialized;
        this._set_initialized_called = false;
        this._set_disposed_called = false;
        this._event_handlers = new Map<EventSubscription, HandlerInfo>();
    }

    //region Object management methods

    /**
     * Starts object initialization
     */
    public initialize(): void{
        if(this._state !== InitializableObjectState.Uninitialized)
            return;

        this._state = InitializableObjectState.Initializing;
        this.logger.debug(`Initializing`);

        setImmediate(async () => {
            this.emitSync('initializing');

            await this.callLifecycleMethod(this.onBeforeInitialization);
            await this.callLifecycleMethod(this.onInitialization);

            // Bind all event handlers
            this.bindEventHandlers();

            if(!this.is_deferred_initialization)
                this.setInitialized();
        });
    }

    /**
     * Method SHOULD be called only ONCE to let the initialization process know that subclass have completed inner initialization
     * Will be called automatically if is_deferred_initialization property is set to (boolean) true
     * @see is_deferred_initialization
     * @protected
     */
    protected setInitialized(): void{
        if(this._state !== InitializableObjectState.Initializing || this._set_initialized_called)
            return;

        this._set_initialized_called = true;
        setImmediate(async () => {
            await this.callLifecycleMethod(this.onAfterInitialization);
            await this.callLifecycleMethod(this.onInitialized);

            this._state = InitializableObjectState.Initialized;
            this.logger.debug(`Initialization done`);

            this.emit('initialized');
        });
    }

    /**
     * Starts object disposition
     */
    public dispose(): void{
        if(this._state !== InitializableObjectState.Initialized)
            return;

        this._state = InitializableObjectState.Disposing;
        this.logger.debug(`Disposing`);

        setImmediate(async () => {
            this.emitSync('disposing');

            await this.callLifecycleMethod(this.onBeforeDisposition);
            await this.callLifecycleMethod(this.onDisposition);

            if(!this.is_deferred_disposition)
                this.setDisposed();
        });
    }

    /**
     * Method SHOULD be called only ONCE to let the disposition process know that subclass have completed inner disposition
     * Will be called automatically if is_deferred_disposition property is set to (boolean) true
     * @see is_deferred_disposition
     * @protected
     */
    protected setDisposed(): void{
        if(this._state !== InitializableObjectState.Disposing || this._set_disposed_called)
            return;

        this._set_disposed_called = true;
        setImmediate(async () => {
            await this.callLifecycleMethod(this.onAfterDisposition);
            await this.callLifecycleMethod(this.onDisposed);

            this._state = InitializableObjectState.Disposed;
            this.logger.debug(`Disposed`);

            // We should emit this event in a synchronous way because then we will dispose event emitter
            this.emitSync('disposed');

            this.unbindEventHandlers();
            this._event_handlers.clear();
            this.emitter.removeAllListeners();

            //@ts-ignore
            delete this['emitter'];
            //@ts-ignore
            delete this['logger'];
        });
    }

    //endregion

    //region Initialization data methods

    /**
     * Method SHOULD return default options
     * @return {Required<TOpts>}
     * @protected
     */
    protected defaultOptions(): Required<TOpts>{
        return {} as Required<TOpts>;
    }

    //endregion

    //region Lifecycle methods

    /**
     * Method is called BEFORE actual initialization is happens (called only once)
     * A good place to initialize object properties
     * @see onInitialization
     * @returns {void|Promise<void>>}
     * @protected
     */
    protected onBeforeInitialization(): Promise<void>{
        return Promise.resolve();
    }

    /**
     * Method called DURING the initialization process, letting subclasses to initialize their internal state
     * @returns {void|Promise<void>}
     * @protected
     */
    protected onInitialization(): Promise<void>{
        return Promise.resolve();
    }

    /**
     * Method is called right AFTER the initialization process but BEFORE .onInitialized method
     * @see onInitialized
     * @returns {void|Promise<void>>}
     * @protected
     */
    protected onAfterInitialization(): Promise<void>{
        return Promise.resolve();
    }

    /**
     * Method is called when the initialization process is done and the object is assumed INITIALIZED
     * @protected
     */
    protected onInitialized(): void{}

    /**
     * Method is called BEFORE actual disposition is happens (called only once)
     * @protected
     */
    protected onBeforeDisposition(): void{};

    /**
     * Method is called DURING the disposition process, letting subclasses to dispose used resources
     * @protected
     */
    protected onDisposition(): void{};

    /**
     * Method is called right after the disposition process but BEFORE .onDisposed method
     * @see onDisposed
     * @protected
     */
    protected onAfterDisposition(): void{};

    /**
     * Method is called when the disposition process is done and the object is assumed DISPOSED
     * @protected
     */
    protected onDisposed(): void{};

    //endregion

    //region Public API

    /**
     * Allows to run a callback whenever the object is initialized.
     * Executes callback immediately if object already initialized.
     * Throws an error if object disposing or disposed
     * @param {VoidCallback} callback
     * @throws {Error}
     */
    public whenInitialized(callback: VoidCallback): void{
        if(this._state === InitializableObjectState.Initialized)
            return callback();

        if(this._state === InitializableObjectState.Disposing || this._state === InitializableObjectState.Disposed)
            throw new Error('Cannot execute callback when object will be initialized. Object currently disposing or already disposed');

        this.once('initialized', callback);
    }

    /**
     * Allows to wait until the object will be initialized.
     * Immediately resolves if the object is already initialized.
     * Immediately rejects if the object is disposing or disposed.
     * Also you can listen to 'initialized' event
     * @see isInitialized
     * @return {Promise<void>}
     */
    public waitInitialized(): Promise<void>{
        if(this._state === InitializableObjectState.Disposing || this._state === InitializableObjectState.Disposed)
            return Promise.reject();

        if(this._state === InitializableObjectState.Initialized)
            return Promise.resolve();

        return new Promise(resolve => this.once('initialized', resolve));
    }

    /**
     * Allows to run a callback whenever the object is disposed
     * Executes callback immediately if the object already disposed
     * @param {VoidCallback} callback
     */
    public whenDisposed(callback: VoidCallback): void{
        if(this._state === InitializableObjectState.Disposed)
            return callback();

        this.once('disposed', callback);
    }

    /**
     * Allows to wait until the object will be disposed.
     * Immediately resolves if the object is already disposed
     * Also you can listen to 'disposed' event
     * @see isDisposed
     * @return {Promise<void>}
     */
    public waitDisposed(): Promise<void>{
        if(this._state === InitializableObjectState.Disposed)
            return Promise.resolve();

        return new Promise(resolve => this.once('disposed', resolve));
    }

    //endregion

    //region Internal API

    /**
     * @param {T} method
     * @param {any[]} args
     * @param thisArg
     * @return {Promise<ReturnType<T>>}
     * @protected
     */
    protected callLifecycleMethod<T extends AnyCallback = AnyCallback>
    (method: T, args: any[] = [], thisArg?: this|any): Promise<ReturnType<T>>{
        this.logger.debug(`Calling lifecycle method ${method.name}`);
        return callFn<T>(method, args, this);
    }

    //endregion

    //region Utility methods


    /**
     * @param {T} options
     * @return {Required<T>}
     * @private
     */
    private mergeOptions(options?: TOpts): Required<TOpts>{
        if(!options || typeof options !== 'object')
            options = {} as TOpts;

        return _merge({}, this.defaultOptions(), options);
    }

    /**
     * @private
     */
    private bindEventHandlers(): void{
        const subscriptions = getEventsMetadata(this);

        for(const subscription of subscriptions){
            //@ts-ignore
            const event_handler = this[subscription.key] || null;
            if(event_handler === undefined || event_handler === null || typeof event_handler !== 'function') {
                this.logger.notice(`Cannot set handler for the event ${String(subscription.event)} on target ${String(subscription.target)}.`
                    + ` Handler ${String(subscription.key)} does not exists on class ${this.constructor.name}`);
                continue;
            }

            //@ts-ignore
            const event_source = this[subscription.target] || null;
            if(event_source === undefined || event_source === null || !isEmitterInterfaceCompatible(event_source)){
                this.logger.notice(`Cannot set handler for the event ${String(subscription.event)} on target ${String(subscription.target)}.`
                    + ` Target ${String(subscription.target)} does not exists on class ${this.constructor.name} or is not compatible with the EmitterInterface`);
                continue;
            }

            const handler = event_handler.bind(this);
            this._event_handlers.set(subscription, {source: event_source, handler: handler});
            event_source[subscription.type](String(subscription.event), handler);
        }
    }

    /**
     * @private
     */
    private unbindEventHandlers(): void{
        for(const [subscription, {source, handler}] of this._event_handlers)
            source['off'](String(subscription.event), handler);
    }

    //endregion

    //region Events

    protected emitSync: TEmitEvents & EmitEvents;
    protected emit: TEmitEvents & EmitEvents;
    public on: TSubscribeEvents & SubscribeEvents;
    public once: TSubscribeEvents & SubscribeEvents;
    public off: TSubscribeEvents & SubscribeEvents;

    //endregion

}

type EmitEvents = {
    (event: 'initializing'): void;
    (event: 'initialized'): void;
    (event: 'disposing'): void;
    (event: 'disposed'): void;
}

type SubscribeEvents = {
    (event: 'initializing', handler: VoidCallback): void;
    (event: 'initialized', handler: VoidCallback): void;
    (event: 'disposing', handler: VoidCallback): void;
    (event: 'disposed', handler: VoidCallback): void;
}

type HandlerInfo = {
    source: IEmitter;
    handler: AnyCallback;
};
