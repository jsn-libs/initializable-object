import 'reflect-metadata';
import {SubscriptionPrefix, SubscribeTo, InitializableObject} from './';
import {getEventsMetadata} from './utils';

class Base extends InitializableObject{

    protected readonly is_deferred_initialization = false;
    protected readonly is_deferred_disposition = false;

    public constructor(){
        super({
            debug(message: string, ...args) {
                console.log(message, ...args);
            },
            info(message: string, ...args) {
                console.log(message, ...args);
            },
            notice(message: string, ...args) {
                console.log(message, ...args);
            },
            warning(message: string, ...args) {
                console.log(message, ...args);
            },
            error(message: string, ...args) {
                console.log(message, ...args);
            }
        });
    }

}

@SubscriptionPrefix('emitter', 'rooms')
class Mod extends Base{

    @SubscribeTo('emitter', 'disconnecting')
    private _onDisconnecting(){

    }

    private _onDisconnected(){

    }

    @SubscribeTo('emitter', 'creation')
    private onCreation(){

    }

}

@SubscriptionPrefix('socket', 'latency')
class LatencyMeasurer extends Mod{

    @SubscribeTo('emitter', 'ping')
    private onPing(){

    }

}

const lm = new LatencyMeasurer(),
    lm2 = new LatencyMeasurer(),
    lm3 = new LatencyMeasurer();


(async () => {
    lm3.initialize();
    await lm3.waitInitialized();

    console.log(lm3);
    console.log(lm3._id, lm3._id_sha1);
})();
