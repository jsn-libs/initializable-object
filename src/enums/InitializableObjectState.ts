export enum InitializableObjectState{

    Uninitialized = 'Uninitialized',
    Initializing = 'Initializing',
    Initialized = 'Initialized',
    Disposing = 'Disposing',
    Disposed = 'Disposed'

}
