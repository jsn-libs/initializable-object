import * as EventEmitter from 'events';
import {AnyCallback, ScheduleFunction} from './callbacks';

export abstract class EventsAware{

    /**
     * @type {EventEmitter}
     * @protected
     */
    protected readonly emitter: EventEmitter;

    /**
     * @type {ScheduleFunction | null}
     * @private
     */
    private scheduleFn: ScheduleFunction|null;

    /**
     * @param {ScheduleFunction | null} scheduleFn
     */
    public constructor(scheduleFn: ScheduleFunction|null = null) {
        this.emitter = new EventEmitter();
        this.scheduleFn = scheduleFn;

        this.emit = this.emit.bind(this);
    }

    /**
     * @param {string} event
     * @param args
     * @protected
     */
    protected emitSync(event: string, ...args: any[]): void{
        this.emitter.emit(event, ...args);
    }

    /**
     * @param {string} event
     * @param args
     * @protected
     */
    protected emit(event: string, ...args: any[]): void{
        this.scheduleFn !== null
            ? this.scheduleFn(() => this.emitter.emit(event, ...args))
            : this.emitter.emit(event, ...args);
    }

    /**
     * @param {string} event
     * @param {AnyCallback} handler
     */
    public on(event: string, handler: AnyCallback): void{
        this.emitter.on(event, handler);
    }

    /**
     * @param {string} event
     * @param {AnyCallback} handler
     */
    public once(event: string, handler: AnyCallback): void{
        this.emitter.once(event, handler);
    }

    /**
     * @param {string} event
     * @param {AnyCallback} handler
     */
    public off(event: string, handler: AnyCallback): void{
        this.emitter.off(event, handler);
    }

    /**
     * @param {string} event
     */
    public offAny(event?: string): void{
        this.emitter.removeAllListeners(event);
    }

}
